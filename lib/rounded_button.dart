part of joco_utils;

class EroundedButton extends StatelessWidget {
  final Color color;
  final Function onPressed;
  final Widget child;
  final Color disabledColor;
  final BorderRadius borderRadius;
  EroundedButton(
      {@required this.color,
      @required this.onPressed,
      @required this.child,
      this.disabledColor,
      this.borderRadius});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: FlatButton(
            disabledColor: disabledColor == null ? Colors.grey : disabledColor,
            shape: RoundedRectangleBorder(
              borderRadius: borderRadius == null
                  ? BorderRadius.circular(18.0)
                  : borderRadius,
            ),
            color: color,
            onPressed: onPressed,
            child: child,
          ),
        ),
      ],
    );
  }
}
