part of joco_utils;

Widget teks(String data,
    {Color color,
    TextDecoration decoration,
    double fontSize,
    FontWeight fontWeight}) {
  if (color == null) color = Colors.black;
  if (decoration == null) decoration = TextDecoration.none;
  if (fontWeight == null) fontWeight = FontWeight.normal;
  return Text(data,
      style: TextStyle(
          color: color,
          decoration: decoration,
          fontSize: fontSize,
          fontWeight: fontWeight));
}
