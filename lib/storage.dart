part of joco_utils;

Future<void> saveData(String key, String value) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setString(key, value);
}

Future<String> getData(String key) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  return _prefs.getString(key);
}

Future<void> removeData(String key) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  _prefs.remove(key);
}

Future<void> removeAllData() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  _prefs.clear();
}

String encodeBase64(String plaintext) {
  final bytes = utf8.encode(plaintext);
  return base64Encode(bytes);
}

String decodeBase64(String plaintext) {
  final string = base64Decode(plaintext);
  return utf8.decode(string);
}
