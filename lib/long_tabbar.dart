part of joco_utils;

StreamController<int> _streamController;

class LongTabView extends StatefulWidget {
  final List<Widget> children;
  LongTabView({@required this.children});
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends State<LongTabView> {
  int _totalview;
  @override
  void initState() {
    _totalview = widget.children.length;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: _streamController.stream,
        initialData: 0,
        builder: (context, snapshot) {
          if (snapshot.data >= _totalview) return Container();
          return widget.children[snapshot.data];
        });
  }
}

class LongTabbar extends StatefulWidget {
  final Color barcolor;
  final Color activecolor;
  final Color linecolor;
  final Function onChange;
  final List<String> titles;
  LongTabbar(
      {this.barcolor,
      this.activecolor,
      this.linecolor,
      this.onChange,
      @required this.titles});
  @override
  _TabState createState() => _TabState();
}

class _TabState extends State<LongTabbar> {
  int _tabke;
  @override
  void initState() {
    _tabke = 0;
    _streamController = StreamController.broadcast();
    super.initState();
  }

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  List<Widget> _tabbar() {
    List<Widget> _list = [];
    for (int _index = 0; _index < widget.titles.length; _index++) {
      final _isactive = _index == _tabke;
      final _s = widget.titles[_index];
      _list.add(
        InkResponse(
          onTap: () {
            _tabke = _index;
            _streamController.sink.add(_index);
          },
          child: Container(
            height: 40,
            padding: const EdgeInsets.all(12.0),
            decoration: _isactive
                ? BoxDecoration(
                    border: Border(
                        bottom:
                            BorderSide(color: widget.activecolor, width: 3)))
                : null,
            child: Text(
              _s,
              style: TextStyle(
                  fontSize: 12,
                  color: widget.linecolor,
                  fontWeight: _isactive ? FontWeight.w700 : FontWeight.w400),
            ),
          ),
        ),
      );
    }
    return _list;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 40,
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: widget.barcolor))),
        ),
        Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: StreamBuilder<int>(
                stream: _streamController.stream,
                initialData: _tabke,
                builder: (context, snapshot) {
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: _tabbar(),
                    ),
                  );
                }))
      ],
    );
  }
}
