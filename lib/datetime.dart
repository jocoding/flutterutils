part of joco_utils;

String getToday() {
  final day = DateFormat('EEEE').format(DateTime.now());
  String _hari = '';
  switch ('$day') {
    case 'Monday':
      _hari = 'Senin';
      break;
    case 'Tuesday':
      _hari = 'Selasa';
      break;
    case 'Wednesday':
      _hari = 'Rabu';
      break;
    case 'Thursday':
      _hari = 'Kamis';
      break;
    case 'Friday':
      _hari = 'Jumat';
      break;
    case 'Saturday':
      _hari = 'Sabtu';
      break;
    case 'Sunday':
      _hari = 'Minggu';
      break;
  }
  return _hari;
}

String todate(int unixtime) {
  DateTime dateTime = DateTime.now();
  RegExp regExp = new RegExp("[a-zA-Z]+");
  final DateTime tanggal = new DateTime.fromMillisecondsSinceEpoch(
      (unixtime * 1000) + dateTime.timeZoneOffset.inMilliseconds);
  final String formattedtgl =
      new DateFormat("dd MMM yyyy kk:mm").format(tanggal);
  Iterable<Match> matches = regExp.allMatches(formattedtgl);
  String smatch;
  for (Match match in matches) {
    smatch = match.group(0);
  }
  final Map<String, String> blnid = {
    'Jan': 'Jan',
    'Feb': 'Peb',
    'Mar': 'Mar',
    'Apr': 'Apr',
    'May': 'Mei',
    'Jun': 'Jun',
    'Jul': 'Jul',
    'Aug': 'Agu',
    'Sep': 'Sep',
    'Oct': 'Okt',
    'Nov': 'Nop',
    'Dec': 'Des'
  };
  if (blnid[smatch] is String) {
    return formattedtgl.replaceFirst(new RegExp(smatch), blnid[smatch]);
  }
  return formattedtgl;
}

int unixtime() {
  return (new DateTime.now().millisecondsSinceEpoch / 1000).floor();
}

int datetounix(DateTime _tgl) {
  return (_tgl.millisecondsSinceEpoch / 1000).floor();
}

String tglwkt(DateTime _tgl) {
  final _unixtime = datetounix(_tgl);
  return todate(_unixtime);
}

String tglonly(DateTime _tgl) {
  final _unixtime = datetounix(_tgl);
  final List _tgls = todate(_unixtime).split(' ');
  return '${_tgls[0]} ${_tgls[1]} ${_tgls[2]}';
}

String tglUS(DateTime _tgl) {
  return new DateFormat("yyyy-MM-dd").format(_tgl);
}
