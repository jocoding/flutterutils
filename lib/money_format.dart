part of joco_utils;

String moneyFormat(dynamic angka) {
  double _angka = double.parse('$angka');
  final _numfor = new NumberFormat("#,##0.00", "en_US");
  return '${_numfor.format(_angka)}'
      .replaceAllMapped(new RegExp(r'.([0-9]{2})$'), (match) => ';${match[1]}')
      .replaceAll(new RegExp(r';00$'), '')
      .replaceAll(new RegExp(r','), '.')
      .replaceAll(new RegExp(r';'), ',');
}
