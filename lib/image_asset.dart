part of joco_utils;

bool _issvg(String _icon) {
  final ekstensi = (_icon.substring(_icon.length - 4)).toLowerCase();
  if (ekstensi == '.svg') return true;
  return false;
}

bool _isnetwork(String _icon) {
  if (_icon.substring(0, 8) == 'https://') return true;
  if (_icon.substring(0, 7) == 'http://') return true;
  return false;
}

Widget imgAsset(String _asset,
    {double height, double width, BoxFit fit, Color color}) {
  if (_isnetwork(_asset))
    return Image.network(_asset,
        height: height, fit: fit == null ? BoxFit.contain : fit, color: color);
  if (_issvg(_asset))
    return SvgPicture.asset(
      _asset,
      height: height,
      width: width,
      fit: fit == null ? BoxFit.contain : fit,
      color: color,
    );
  return Image.asset(
    _asset,
    height: height,
    width: width,
    fit: fit == null ? BoxFit.contain : fit,
    color: color,
  );
}
